Introduction
------------
ECA Variety Pack is a module that provides a miscellaneous collection of events, conditions and actions that don't perhaps fit in a specific category.

Installation
------------
Install using composer in the usual fashion:

`composer require drupal/eca_variety_pack`

